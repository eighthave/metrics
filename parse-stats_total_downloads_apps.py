#!/usr/bin/env python3
#
#

import fdroid_metrics
import glob
import json
import os
from datetime import datetime


last_value = dict()
report = dict()
for f in sorted(glob.glob("per-commit/*.txt")):
    timestamp = int(os.path.basename(f).split("-")[0])
    dt = datetime.fromtimestamp(timestamp)
    weekStartDate = fdroid_metrics.get_week_start_date(dt)
    if weekStartDate not in report:
        per_app_ids = dict()
        report[weekStartDate] = {"perApplicationID": per_app_ids}
    with open(f) as fp:
        for line in fp:
            if line.startswith("#"):
                continue
            appid, downloads = line.split()
            downloads = int(downloads)
            last_value[appid] = downloads
            if appid == "ALL":
                report[weekStartDate]["ALL"] = downloads
            else:
                per_app_ids[appid] = downloads

previous = {}
for weekStartDate in report:
    current = report[weekStartDate]
    current["ALL"] = max(0, current["ALL"] - previous.get("ALL", 0))
    previous["ALL"] = current["ALL"]
    for appid in current.get("perApplicationID", {}).keys():
        previous_downloads = previous.get(appid, 0)
        current["perApplicationID"][appid] = max(
            0, current["perApplicationID"][appid] - previous_downloads
        )
        previous[appid] = current["perApplicationID"][appid]

for weekStartDate, hit_bucket in report.items():
    date = weekStartDate.strftime('%Y-%m-%d')
    with open(fdroid_metrics.get_hit_bucket_file_name('per-commit', date), 'w') as fp:
        json.dump(
            hit_bucket,
            fp,
            cls=fdroid_metrics.Encoder,
            indent=2,
            sort_keys=True,
            ensure_ascii=False,
        )
