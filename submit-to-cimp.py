#!/usr/bin/env python3

import fdroid_metrics
import glob
import json
import os
import requests
import sys
import time
from datetime import datetime, timezone
from jsonschema import validate, ValidationError, SchemaError

WEEK_IN_SECONDS = 7 * 24 * 60 * 60


def get_idsite():
    site = os.path.basename(os.getcwd())
    if site == 'f-droid.org':
        return 6
    if site == 'mirror.f-droid.org':
        return 7
    if site == 'search.f-droid.org':
        return 8
    if site == 'staging.f-droid.org':
        return 9
    if site == 'verification.f-droid.org':
        return 10
    if site == 'example.com':
        return 5
    raise Exception('ERROR: Unknown site "%s"' % site)


def add_report_template(lang):
    if lang not in reports:
        reports[lang] = {
            "idsite": get_idsite(),
            "ua": os.path.basename(__file__),
            "visits": [],
            "events": [],
        }
        if lang != '-':
            reports[lang]['lang'] = lang


def get_event(category, action, times, name=None):
    event = {
        "category": category,
        "action": action,
        "period_start": weekStartDate,
        "period_end": weekStartDate + WEEK_IN_SECONDS,
        "times": times,
    }
    if name:
        event['name'] = name
    return event


if len(sys.argv) > 1 and sys.argv[1] == 'post':
    post = True
else:
    post = False

# https://gitlab.com/cleaninsights/clean-insights-design/-/raw/master/schemas/cimp.schema.json
with open(os.path.join(os.path.dirname(__file__), 'cimp.schema.json')) as fp:
    schema = json.load(fp)

if os.path.exists('last_submitted_to_cimp.json'):
    with open('last_submitted_to_cimp.json') as fp:
        last_submitted_to_cimp = json.load(fp)['last_submitted_to_cimp']
else:
    last_submitted_to_cimp = 0

errors = 0
for f in sorted(glob.glob(fdroid_metrics.DATA_JSON_GLOB)):
    with open(f) as fp:
        data = json.load(fp)

    # The week when this script is run is by definition incomplete.
    incomplete_week_start = fdroid_metrics.get_week_start_date(datetime.now(timezone.utc))
    if datetime.fromisoformat(data['firstProcessedDate']) >= incomplete_week_start:
        print('skipping %s, the reporting period is incomplete' % f)
        continue

    weekStartDate = int(datetime.fromisoformat(data['weekStartDate']).timestamp())
    if last_submitted_to_cimp < weekStartDate:
        last_submitted_to_cimp = weekStartDate
    else:
        print('skipping %s, already submitted to CIMP' % f)
        continue

    reports = dict()
    add_report_template('-')
    catch_all_report = reports['-']

    for path, hitsdict in data['paths'].items():
        total = hitsdict['hits']
        for lang, hits in hitsdict.get('hitsPerLanguage', {}).items():
            if not lang:
                continue
            add_report_template(lang)
            reports[lang]['visits'].append(
                {
                    "action_name": path,
                    "times": hits,
                    "period_start": weekStartDate,
                    "period_end": weekStartDate + WEEK_IN_SECONDS,
                }
            )
            total -= hits
        catch_all_report['visits'].append(
            {
                "action_name": path,
                "times": total,
                "period_start": weekStartDate,
                "period_end": weekStartDate + WEEK_IN_SECONDS,
            }
        )

    for path, hitsdict in data['paths'].items():
        total = hitsdict['hits']
        for country, hits in hitsdict.get('hitsPerCountry', {}).items():
            if not country:
                continue
            catch_all_report['events'].append(
                get_event('perPathHitsPerCountry', path, hits, name=country)
            )

    total = data['hits']
    for country, hits in data.get('hitsPerCountry', {}).items():
        if not country:
            continue
        catch_all_report['events'].append(get_event('hitsPerCountry', country, hits))
        total -= hits
    if total > 0:
        catch_all_report['events'].append(get_event('hitsPerCountry', '?', total))

    total = data['hits']
    for language, hits in data.get('hitsPerLanguage', {}).items():
        if not language:
            continue
        catch_all_report['events'].append(get_event('hitsPerLanguage', language, hits))
        total -= hits
    if total > 0:
        catch_all_report['events'].append(get_event('hitsPerLanguage', '-', total))

    for query, hitsdict in data.get('queries', {}).items():
        if not query:
            continue
        catch_all_report['events'].append(get_event('query', query, hitsdict['hits']))

    tmpdir = os.path.join(
        '/tmp', 'submit-to-cimp', os.path.basename(os.getcwd()), f[:-5]
    )
    os.makedirs(tmpdir, exist_ok=True)

    for lang, report in reports.items():
        try:
            if validate(report, schema) is not None:
                print('ERROR: JSON did not validate')
                errors += 1
        except (SchemaError, ValidationError) as e:
            print('ERROR', lang, e.__class__.__name__, e, sep=': ')
            errors += 1
        with open(os.path.join(tmpdir, lang + '.json'), 'w') as fp:
            json.dump(report, fp, indent=2, sort_keys=True)

    if errors == 0 and post:
        for lang, report in reports.items():
            print('POST', lang)
            while True:
                try:
                    r = requests.post(
                        'https://metrics.cleaninsights.org/cleaninsights.php',
                        json=report,
                        timeout=120,
                    )
                    print(r.content)
                    r.raise_for_status()
                    break
                except Exception as e:
                    print('retry', lang, e)
                    time.sleep(60)
        with open('last_submitted_to_cimp.json', 'w') as fp:
            json.dump({'last_submitted_to_cimp': last_submitted_to_cimp}, fp)

sys.exit(errors)
